namespace GfKSurvey.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using GfKSurvey.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<GfKSurvey.Models.GfKSurveyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        /// <summary>
        /// This seed method is used to populate initial date
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(GfKSurvey.Models.GfKSurveyContext context)
        {


            context.Questions.AddOrUpdate(x => x.Id,
     new Question() { Id = 1, QuestionType = "Single", Text = "How did you find out about this job opportunity?" },
     new Question() { Id = 2, QuestionType = "Multiple", Text = "How do you find the company�s location?" },
     new Question() { Id = 3, QuestionType = "Single", Text = "What was your impression of the office where you had the interview?" },
     new Question() { Id = 4, QuestionType = "Single", Text = "How technically challenging was the interview?" },
     new Question() { Id = 5, QuestionType = "Multiple", Text = "How can you describe the manager that interviewed you?" }
     );

            context.Answers.AddOrUpdate(x => x.Id,
            new Answer() { Id = 1, QuestionId = 1, Text = "StackOverflow" },
            new Answer() { Id = 2, QuestionId = 1, Text = "Indeed" },
            new Answer() { Id = 3, QuestionId = 1, Text = "Other" },
            new Answer() { Id = 4, QuestionId = 2, Text = "Easy to access by public transport" },
            new Answer() { Id = 5, QuestionId = 2, Text = "Easy to access by car" },
            new Answer() { Id = 6, QuestionId = 2, Text = "In a pleasant area" },
            new Answer() { Id = 7, QuestionId = 2, Text = "None of the above" },
            new Answer() { Id = 8, QuestionId = 3, Text = "Tidy" },
            new Answer() { Id = 9, QuestionId = 3, Text = "Sloppy" },
            new Answer() { Id = 10, QuestionId = 3, Text = "Did not notice" },
            new Answer() { Id = 11, QuestionId = 4, Text = "Very difficult" },
            new Answer() { Id = 12, QuestionId = 4, Text = "Difficult" },
            new Answer() { Id = 13, QuestionId = 4, Text = "Moderate" },
            new Answer() { Id = 14, QuestionId = 4, Text = "Easy" },
            new Answer() { Id = 15, QuestionId = 5, Text = "Enthusiastic" },
            new Answer() { Id = 16, QuestionId = 5, Text = "Polite" },
            new Answer() { Id = 17, QuestionId = 5, Text = "Organized" },
            new Answer() { Id = 18, QuestionId = 5, Text = "Could not tell" });

            context.Responses.AddOrUpdate(x => x.Id,
                new Response() { Id = 1, QuestionId = 1, ResponseAnswerId = 1 },
                new Response() { Id = 2, QuestionId = 2, ResponseAnswerId = 1 },
                new Response() { Id = 3, QuestionId = 2, ResponseAnswerId = 2 },
                new Response() { Id = 4, QuestionId = 3, ResponseAnswerId = 1 },
                new Response() { Id = 5, QuestionId = 4, ResponseAnswerId = 2 },
                new Response() { Id = 6, QuestionId = 5, ResponseAnswerId = 2 },
                new Response() { Id = 7, QuestionId = 5, ResponseAnswerId = 3 }
                );
            
        }
    }
}
