﻿QuestionsViewModel = function (data) {
    var self = this;
    self.Questions = ko.observableArray([]);
    self.GetQuestions = function () {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "api/Questions",
            success: function (data) {
                self.Questions($.map(data, function (answers) {
                    return new AnswersViewModel(answers);
                }));
            }
        })
    }

    function AnswersViewModel(data) {
        var self = this;
        self.answers = ko.observableArray(data.answers);
    }
    self.GetQuestions();
    ko.applyBindings(new QuestionsViewModel());
}