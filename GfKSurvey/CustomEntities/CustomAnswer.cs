﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GfKSurvey.CustomEntities
{
    public class CustomAnswer
    {
        public int Id { get; set; }
        public string Text { get; set; }

    }
}