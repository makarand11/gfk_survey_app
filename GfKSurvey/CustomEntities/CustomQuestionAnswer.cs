﻿using GfKSurvey.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace GfKSurvey.CustomEntities
{

    [JsonConverter(typeof(ResponseConverter))]
    public class CustomQuestionAnswer
    {
        public int Id { get; set; }
        public string QuestionText { get; set; }
        public string QuestionType { get; set; }

        public IQueryable<CustomAnswer> Answers { get; set; }
    }
}