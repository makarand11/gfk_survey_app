﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GfKSurvey.CustomEntities;
namespace GfKSurvey.Models
{
    public class ResponseConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType) => objectType == typeof(Response);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) => throw new NotImplementedException();

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            CustomQuestionAnswer originalResponse = (CustomQuestionAnswer)value;
            writer.WriteStartObject();
            writer.WritePropertyName(nameof(originalResponse.Id));
            writer.WriteValue(originalResponse.Id);


            writer.WritePropertyName(nameof(originalResponse.QuestionText));
            writer.WriteValue(originalResponse.QuestionText);


            writer.WritePropertyName(nameof(originalResponse.QuestionType));
            writer.WriteValue(originalResponse.QuestionType);

            writer.WritePropertyName(nameof(originalResponse.Answers));
            writer.WriteStartArray();
            foreach (var val in originalResponse.Answers)
            {
                writer.WriteStartObject();
                //writer.WriteStartArray();
                writer.WritePropertyName(nameof(val.Id));
                writer.WriteValue(val.Id);
                writer.WritePropertyName(nameof(val.Text));
                writer.WriteValue(val.Text);
                // writer.WriteEndArray();
                writer.WriteEndObject();
            }
            writer.WriteEndArray();
            writer.WriteEndObject();
        }
    }
}