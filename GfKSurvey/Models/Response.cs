﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GfKSurvey.Models
{
    public class Response
    {
        public int Id { get; set; }
        //Foreign key
        public int QuestionId { get; set; }
        
        //Navigation Property
       // public Question Question { get; set; }
        
        //Foreign key
        public int ResponseAnswerId { get; set; }
        
        //Navigation Property
        //public Answer ResponseAnswer { get; set; }
    }
}