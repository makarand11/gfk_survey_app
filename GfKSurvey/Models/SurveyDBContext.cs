﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GfKSurvey.CustomEntities;
using GfKSurvey.DAL;
using GfKSurvey.DAL.DTO;

namespace GfKSurvey.Models
{
    /// <summary>
    /// Additonal simple db context for uncovering issues with EF
    /// </summary>
         
    public class SurveyDBContext
    {
        public List<CustomQuestionAnswer> GetQuestions()
        {
            DataAccess access = new DataAccess();
            List<QuestionDTO> questionList = access.GetQuestionsFromDB();
            List<AnswerDTO> answerList = access.GetAnswersFromDB();
            var ques = from q in questionList
                       select new CustomQuestionAnswer()
                       {
                           Id = q.Id,
                           QuestionText   = q.QuestionText,
                           Answers = (from a in answerList.Where(a=> a.QuestionId == q.Id)
                                     select new CustomAnswer()
                                     { Id = a.Id,
                                      Text=a.AnswerText
                                     }).AsQueryable()
                       };
            return ques.ToList();   
        }

       
    }
            
}
