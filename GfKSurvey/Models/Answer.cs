﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace GfKSurvey.Models
{
    public class Answer
    {
        public int Id { get; set; }

        [Required]
        public string Text { get; set; }
        
        
        public int QuestionId { get; set; }
        
        //navigation property
       // public Question Question { get; set; }

    }
}