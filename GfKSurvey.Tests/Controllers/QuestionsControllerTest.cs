﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GfKSurvey.Controllers;
using System.Linq;
using GfKSurvey.CustomEntities;

namespace GfKSurvey.Tests.Controllers
{
        [TestClass]
        public class QuestionsControllerTest
        {
            [TestMethod]
            public void Get()
            {
                // Arrange
                QuestionsController controller = new QuestionsController();

            // Act
            IQueryable<CustomQuestionAnswer> result = controller.GetQuestions();

                // Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(5, result.Count());
                
            }
        }
}
