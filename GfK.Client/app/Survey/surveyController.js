﻿/**
 * Created by Makarand on 10/08/2019.
 */
(function () {
    "use strict";
    angular
        .module("surveyModule")
        .controller("SurveyController",
            SurveyController);
               
    function SurveyController() {
        var vm = this;
        
        //surveyResource.get( function(data) {
          //  vm.questions = data;
        //});
        //Hardcoded due to error in getting answers array in json response.
        vm.questions = [
            {
                "Id": 1,
                "QuestionText": 'How did you find out about this job opportunity?',
                "Type": "Single",
                "answers": [
                    {
                        "Id": 1,
                        "Text": "StackOverflow"
                    },
                    {
                        "Id": 2,
                        "Text": "Indeed"
                    },
                    {
                        "Id": 3,
                        "Text": "Other"
                    }
                    ]
            },
            {
                "Id": 2,
                "QuestionText": "How do you find the company’s location?",
                "Type": "Multiple",
                "answers": [
                    {
                        "Id": 1,
                        "Text": "Easy to access by public transport"
                    },
                    {
                        "Id": 2,
                        "Text": "Easy to access by car"
                    },
                    {
                        "Id": 3,
                        "Text": "In a pleasant area"
                    },
                    {
                        "Id": 4,
                        "Text": "None of the above"
                    }

                ]
            },
            {
                "Id": 3,
                "QuestionText": "What was your impression of the office where you had the interview?",
                "Type": "Single",
                "answers": [
                    {
                        "Id": 1,
                        "Text": "Tidy"
                    },
                    {
                        "Id": 2,
                        "Text": "Sloppy"
                    },
                    {
                        "Id": 3,
                        "Text": "Did not notice"
                    }
                ]
            },
            {
                "Id": 4,
                "QuestionText": "How technically challenging was the interview?",
                "Type": "Single",
                "answers": [
                    {
                        "Id": 1,
                        "Text": "Very difficult"
                    },
                    {
                        "Id": 2,
                        "Text": "Difficult"
                    },
                    {
                        "Id": 3,
                        "Text": "Moderate"
                    },
                    {
                        "Id": 4,
                        "Text": "Easy"
                    }
                ]
            },
            {
                "Id": 5,
                "QuestionText": "How can you describe the manager that interviewed you?",
                "Type": "Multiple",
                "answers": [
                    {
                        "Id": 1,
                        "Text": "Enthusiastic"
                    },
                    {
                        "Id": 2,
                        "Text": "Polite"
                    },
                    {
                        "Id": 3,
                        "Text": "Organized"
                    },
                    {
                        "Id": 4,
                        "Text": "Could not tell"
                    }

                ]
            }




            
        ]
    }      

    //function SurveyController(questionResource) {
      //  var vm = this;

        //questionResource.query(function (data) {
          //  vm.questions = data;
        //});
        //vm.showImage = false;

        //vm.toggleImage = function () {
          //  vm.showImage = !vm.showImage;
        //}
    
}());
