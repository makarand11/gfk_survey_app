/**
 * Created by Deb on 8/21/2014.
 */
(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("questionResource",
                ["$resource",
                 questionResource]);

    function questionResource($resource) {
        return $resource("/api/questions/")
    }

}());
