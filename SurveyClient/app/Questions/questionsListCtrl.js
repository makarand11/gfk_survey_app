/**
 * Created by Makarand on 10/08/2019.
 */
(function () {
    "use strict";
    angular
        .module("surveyManagement")
        .controller("QuestionListCtrl",
                    ["questionResource",
                        QuestionListCtrl]);

    function QuestionListCtrl(questionResource) {
        var vm = this;

        questionResource.query(function(data) {
            vm.questions = data;
        });
        vm.showImage = false;

        vm.toggleImage = function() {
            vm.showImage = !vm.showImage;
        }
    }
}());
