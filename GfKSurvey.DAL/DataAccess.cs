﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using GfKSurvey.DAL.DTO;
namespace GfKSurvey.DAL
{
    public class DataAccess
    {
       public List<QuestionDTO> GetQuestionsFromDB()
        {
            List<QuestionDTO> questionList = new List<QuestionDTO>();
            using (SqlConnection con = new SqlConnection("Server=localhost\\sqlexpress;Database=GfKSurvey_2;Integrated Security = true"))//Need to move to configurations
            {

                con.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Questions", con))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader != null)
                        {
                            
                            while (reader.Read())
                            {
                                questionList.Add(new QuestionDTO()
                                {
                                    Id = Int32.Parse(reader["QuestionID"].ToString()),
                                    QuestionType = reader["QuestionType"].ToString(),
                                    QuestionText = reader["QuestionText"].ToString()

                                });
                                //do something
                            }
                        }
                    } // reader closed and disposed up here

                } // command disposed here

            }
            return questionList;//conne
        }

        public List<AnswerDTO> GetAnswersFromDB()
        {
            List<AnswerDTO> answerList = new List<AnswerDTO>();
            using (SqlConnection con = new SqlConnection("Server=localhost\\sqlexpress;Database=GfKSurvey_2;Integrated Security = true"))//Need to move to configurations
            {

                con.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Answers", con))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader != null)
                        {

                            while (reader.Read())
                            {
                                answerList.Add(new AnswerDTO()
                                {
                                    Id = Int32.Parse(reader["AnswerID"].ToString()),
                                    AnswerText = reader["AnswerText"].ToString(),
                                    QuestionId = Int32.Parse(reader["QuestionID"].ToString())

                                });
                                //do something
                            }
                        }
                    } // reader closed and disposed up here

                } // command disposed here

            }
            return answerList;//conne
        }


    }
}
